# Movie Database

Welcome to the **Movie Database** project! This repository contains the source code for a web application that allows users to explore, search, and review movies. It's perfect for movie enthusiasts and professionals alike.

## Table of Contents

- [Features](#features)
- [Technologies](#technologies)
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Features

- Browse through an extensive collection of movies
- Search for movies by title, genre, release year, and more
- View detailed movie information, including synopsis, cast, and ratings
- Leave your own ratings and reviews
- Create and manage personal watchlists

## Technologies

- Frontend: React, SASS

## Installation

1. Clone the repository:
	```
	git clone https://gitlab.com/ayes-coding-bootcamp/projects/movie-database.git
	cd movie-database
	```
2. Install dependencies:
	```
	npm install
	```
	or for yarn
	```
	yarn install
	```
3. Start the development server:
	```
	npm run start
	```
 	or for yarn
	```
	yarn start
	```

## Usage

Visit `http://localhost:3000` in your browser to start using the Movie Database web application.

## Contributing

We welcome contributions from the community! Please follow the steps below to get started:

1. Fork the repository and create your branch from `main`.
2. Make your changes and commit them.
3. Push your changes to the forked repository.
4. Create a new pull request on the original repository.

Please ensure that your code follows the project's coding standards and guidelines.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Support

If you encounter any issues or have any questions, feel free to open an issue on the [GitLab repository](https://gitlab.com/ayes-coding-bootcamp/projects/movie-database/issues) or contact the maintainers.

## Maintainers

- [Justin Schildt](mailto:justinsupercode@gmail.com)

## Acknowledgements

- A special thanks to [The Movie Database (TMDb)](https://www.themoviedb.org/) for providing the API used in this project.
- Thanks to all contributors and users for their support and encouragement.