export const sanitizeMovieTitle = title => title
	.toLowerCase()
	.trim()
	.replace(/[^\w\s-]/g, '')
	.replace(/[\s_-]+/g, '-');

export const desanitizeMovieTitle = title => title.replace(/-/g, " ");