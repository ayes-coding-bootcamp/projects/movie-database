import React from 'react';
import styled from 'styled-components';

/* Style */
const StyledButton = styled.button`
  
`;

/* Component */
const Button = (props) => {
	return (
		<StyledButton>
			{props.children}
		</StyledButton>
	);
};

export default Button;
