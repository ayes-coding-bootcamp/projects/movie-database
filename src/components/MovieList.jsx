import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import {sortModes, sortOrders} from '../data/sortModes';
import movieData from '../data/movies';
import MovieItem from './movieItem/MovieItem';
import {Flipper} from 'react-flip-toolkit';
import {Link} from "react-router-dom";
import {sanitizeMovieTitle} from "../utils/helperFunctions";

// Styles
const StyledMovieList = styled.section`
  .movieControls {
	position: sticky;
	top: 0;
	z-index: 1;
	backdrop-filter: blur(16px);
	padding: 1rem;

	background: rgba(0, 0, 20, 0.3);

	form {
	  display: flex;
	  gap: 1rem;

	  label {
		display: flex;
		justify-content: space-evenly;
		align-items: center;

		input {
		  display: inline-block;
		  margin: 4px;
		}
	  }
	}
  }

  .movieContainer {
	display: grid;
	grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
	gap: 1rem;
	padding: 1rem;

	article {
	  border-radius: 8px;
	}

	a {
	  color: inherit;
	  text-decoration: inherit;
	}
  }
`;

// Component
const MovieList = () => {
	const [movies, setMovies] = useState([]);
	const [sortOrder, setSortOrder] = useState(0);
	const [sortMode, setSortMode] = useState(sortModes[0]);
	const [filter, setFilter] = useState('');

	useEffect(() => {
		if (movies.length === 0) {
			setMovies(movieData);
		}
	}, [movies]);

	const filteredMovies = [...movies]
	.sort((itemA, itemB) => sortMode.algorithm(itemA, itemB, sortOrders[sortOrder]))
	.filter((movie) => {
		const filterItems = filter.toLowerCase().split(' ');

		return filterItems.every((item) => {
			return (
				movie.title.toLowerCase().includes(item) ||
				movie.director.toLowerCase().includes(item) ||
				movie.genre.some((g) => g.toLowerCase().includes(item))
			);
		});
	});

	return (
		<StyledMovieList>
			<div className="movieControls">
				<form>
					{sortModes.map((item, index) => (
						<label key={index}>
							<input
								type="radio"
								name="sortMode"
								onChange={() => setSortMode(sortModes[index])}
								checked={sortMode === sortModes[index]}
							/>
							{item.title}
						</label>
					))}

					<select onChange={(event) => setSortOrder(event.target.value)} value={sortOrder}>
						{sortOrders.map((item, index) => (
							<option key={index} value={index}>
								{item.title}
							</option>
						))}
					</select>

					<input type="text" placeholder={"search a movie"}
						   onInput={(event) => setFilter(event.target.value)}/>
				</form>


			</div>

			<Flipper
				className="movieContainer"
				flipKey={`${sortMode.title}${sortOrder}${filter}`}
				spring="veryGentle"
				staggerConfig={{
					default: {
						speed: 50,
					},
				}}
			>
				{filteredMovies.map((movie) => (
					<Link to={`/${sanitizeMovieTitle(movie.title)}`} key={movie.id}>
						<MovieItem movie={movie} stagger={true}/>
					</Link>

				))}
			</Flipper>
		</StyledMovieList>
	);
};

export default MovieList;
