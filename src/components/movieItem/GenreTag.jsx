import React from 'react';
import styled from 'styled-components';

/* Style */
const StyledGenreTag = styled.div`
  font-size: 0.75rem;
  padding: 0.5rem;
  background: rgba(255, 255, 255, 0.075);
`;

/* Component */
const GenreTag = (props) => {
	return (
		<StyledGenreTag>
			{props.genre}
		</StyledGenreTag>
	);
};

export default GenreTag;
