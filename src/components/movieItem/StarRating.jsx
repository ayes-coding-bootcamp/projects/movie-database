import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar, faStarHalfAlt } from '@fortawesome/free-solid-svg-icons';
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons';


/* Style */
const StyledStarRating = styled.div`
  text-align: center;
	padding: 0.5rem 0;
  white-space: nowrap;
`;

/* Component */
const StarRating = ({rating}) => {
	const totalStars = 10;
	const roundedRating = Math.round(rating * 2) / 2;
	const fullStars = Math.floor(roundedRating);
	const halfStars = roundedRating % 1 >= 0.5 ? 1 : 0;
	const emptyStars = totalStars - fullStars - halfStars;
	return (
		<StyledStarRating>
			<p>
				{rating}
			</p>
			{Array(fullStars)
			.fill(0)
			.map((_, i) => (
				<FontAwesomeIcon key={i} icon={faStar}/>
			))}
			{halfStars > 0 && <FontAwesomeIcon icon={faStarHalfAlt}/>}
			{Array(emptyStars)
			.fill(0)
			.map((_, i) => (
				<FontAwesomeIcon key={i} icon={farStar} />
			))}
		</StyledStarRating>
	);
};


export default StarRating;
