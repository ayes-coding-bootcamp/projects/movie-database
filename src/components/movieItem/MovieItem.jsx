import React from 'react';
import styled from 'styled-components';
import GenreTag from "./GenreTag";
import {Flipped} from "react-flip-toolkit";
import StarRating from "./StarRating";


/* Style */
const StyledMovieItem = styled.article`
  padding: 1rem;
  background: rgba(255, 255, 255, 0.1);
  opacity: 1;

  transition: opacity 500ms, background 200ms, scale 200ms;

  backdrop-filter: blur(16px);

  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  gap: 0.2rem;

  box-shadow: rgba(0, 0, 0, 0.5);
  
  min-height: 300px;
  
  &:hover {
	scale: 1.05;
	transform-origin: center center;
	background: rgba(255, 255, 255, 0.2);
  }

  h2 {
	font-weight: 900;
	line-height: 1.6rem;
	text-align: left;
	filter: drop-shadow(0 0 8px rgba(255, 255, 255, 0.5));
	
	width: 70%;
  }
  
  article {
	display: flex;
	justify-content: space-between;
	align-items: center;
  }
  
  .titleBar {
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 0 0 2rem 0;
  }
  
  .genreContainer {
	display: flex;
	flex-wrap: wrap;
	gap: 0.5rem;
  }
`;

/* Component */
const MovieItem: React.FunctionComponent = (props) => {

	return (
		<Flipped
			flipId={props.movie.id}
			stagger
		>
			<StyledMovieItem>
				<article className="titleBar">
					<h2>
						{props.movie.title}
					</h2>

					<p>
						{props.movie.duration}
					</p>
				</article>
				<article>
					<h4>
						{props.movie.director}
					</h4>

					<h5>
						{props.movie.year}
					</h5>
				</article>
				<StarRating rating={props.movie.rate}/>
				<div className="genreContainer">
					{props.movie.genre.map((item, index) => {
						return (
							<GenreTag key={index} genre={item}/>
						);
					})}
				</div>
			</StyledMovieItem>
		</Flipped>
	);
};

export default MovieItem;
