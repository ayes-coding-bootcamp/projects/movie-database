import React from 'react';
import styled from 'styled-components';
import MovieList from "./components/MovieList";
import {Route, Routes} from "react-router-dom";
import MovieDetails from "./components/movieDetails/MovieDetails";

/* Style */
const StyledApp = styled.div`
  font-family: Arial, sans-serif;
`;

/* Component */
const App = () => {
	return (
		<StyledApp>
			<Routes>
				<Route path="/" element={<MovieList/>}/>
				<Route path="/:sanitized-movie-title" element={<MovieDetails/>}/>
			</Routes>
		</StyledApp>
	);
};

export default App;
