export const sortOrders = [
	{
		title: "Sort Ascending",
		algorithm: (a, b) => a - b
	},
	{
		title: "Sort Descending",
		algorithm: (a, b) => b - a
	}
];

export const sortModes = [
	{
		title: "Sort by Release Date",
		algorithm: (itemA, itemB, order) => {
			return order.algorithm(itemA.year, itemB.year);
		}
	},
	{
		title: "Sort by Title",
		algorithm: (itemA, itemB, order) => {
			return itemA.title.localeCompare(itemB.title) * (order === sortOrders[0] ? 1 : -1);
		}
	},
	{
		title: "Sort by Ratings",
		algorithm: (itemA, itemB, order) => {
			return order.algorithm(itemA.rate, itemB.rate);
		}
	},
	{
		title: "Sort by Genre",
		algorithm: (itemA, itemB, order) => {
			const sortedGenresA = itemA.genre.map(g => g.toLowerCase()).sort();
			const sortedGenresB = itemB.genre.map(g => g.toLowerCase()).sort();

			return order.algorithm(sortedGenresA[0].localeCompare(sortedGenresB[0]));
		},
	}
];
