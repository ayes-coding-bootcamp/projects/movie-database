import {createGlobalStyle} from "styled-components";

export const GlobalStyle = createGlobalStyle`
  * {
	padding: 0;
	margin: 0;
	box-sizing: border-box;
  }

  html {

  }

  body {
	background: linear-gradient(17deg,
	rgba(6, 6, 22, 1) 0%,
	rgba(17, 57, 41, 1) 18%,
	rgba(63, 103, 59, 1) 43%,
	rgba(24, 96, 88, 1) 65%,
	rgb(26, 70, 84) 83%,
	rgba(6, 6, 22, 1) 100%);
	color: rgba(255, 255, 255, 1);
	
	background-size: cover;
	
	margin: 0 auto;
	
	overflow-x: hidden;
  }
`;